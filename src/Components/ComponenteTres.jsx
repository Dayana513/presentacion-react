import facebook from '../assets/img/facebook.png'
import youtube from '../assets/img/youtube.png'
import instagram from '../assets/img/instagram.png'

export default function ComponenteTres() {
  return (
    <>
    <br></br>
    <div className='lindo2'>
        <div><br></br>
            <a  href="https://es-la.facebook.com/"><img src={facebook} width="25" height="25"/></a>
        </div>

        <div><br></br>
            <a  href="https://www.youtube.com/"><img src={youtube} width="30" height="30"/></a>
        </div>

        <div><br></br>
            <a href="https://www.instagram.com/"><img src={instagram} width="25" height="25"/> </a>
        </div>
    </div>
    </>
  )
}
