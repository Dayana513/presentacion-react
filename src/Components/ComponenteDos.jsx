import Dayana from '../assets/img/Dayana.jpeg'
import nombre from '../assets/img/nombre.png'
import edad from '../assets/img/edad.png'
import sexo from '../assets/img/sexo.png'
import astrologia from '../assets/img/astrologia.png'
import colegio from '../assets/img/colegio.png'
import maestro from '../assets/img/maestro.png'
import pelicula from '../assets/img/pelicula.png'


export default function ComponenteDos() {
  return (
    <>
        <br></br>
        <div className='lindo'>
            <br></br>
            <img src={Dayana} width="200" height="200"/>
        </div>

    <div className='tcolumnas'>
        <br></br>
        <div>
            <img src={nombre} width="70" height="70"/>
            <h2>Nombre: Dayana Salazar</h2>
        </div>

        <div>
        <br></br>
            <img src={edad} width="70" height="70"/>
            <h2>Edad: 21 años</h2>
        </div>

        <div>
        <br></br>
            <img src={sexo} width="70" height="70"/>
            <h2>Sexo: Mujer</h2>
        </div>

        <div>
        <br></br>
            <img src={astrologia} width="70" height="70"/>
            <h2>Horoscopo: Virgo</h2>
        </div>

        <div>
        <br></br>
            <img src={colegio} width="70" height="70"/>
            <h2>Escuela: Instituto Tecnologico de Cuautla</h2>
        </div>

        <div>
        <br></br>
            <img src={maestro} width="70" height="70"/>
            <h2>Maestro: Ing. Guillermo Urzúa Sánchez</h2>
        </div>

        <div>
        <br></br>
            <img src={pelicula} width="70" height="70"/>
            <h2>Pelicula Favorita: Mulan</h2>
        </div>
    </div>
    </>
  )
}
