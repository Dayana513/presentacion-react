import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'

import ComponenteUno from './Components/ComponenteUno'
import ComponenteDos from './Components/ComponenteDos'
import ComponenteTres from './Components/ComponenteTres'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div>
      <ComponenteUno/>
      <ComponenteDos/>
      <ComponenteTres/>
    </div>
  )
}
export default App
